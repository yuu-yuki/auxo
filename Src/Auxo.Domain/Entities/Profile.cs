using System;
using System.Collections.Generic;

namespace Auxo.Domain.Entities
{
    public class Profile {
        public Guid ProfileId {get; set;}
        public string Username { get; set; }
        public string DisplayName {get; set;}
        public string Email { get; set; }
        public string Bio { get; set; }
        public byte[] Image {get; set;}
        public byte[] Hash { get; set; }
        public byte[] Salt { get; set; }
        public IList<Participant> Teams {get; set;}
    }
}