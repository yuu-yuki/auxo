using System;
using System.Collections.Generic;

namespace Auxo.Domain.Entities
{
    public class Participant {
        public Guid ParticipantId {get; set;}
        public Guid ProfileId {get; set;}
        public Profile Profile {get; set;}
        public Guid TeamId {get; set;}
        public Team Team {get; set;}
    }
}