using System;
using System.Collections.Generic;

namespace Auxo.Domain.Entities
{
    public class Team {
        public Guid TeamId {get; set;}
        public string TeamName {get; set;}
        public string TeamSlug {get; set;}
        public string Description {get; set;}
        public IList<Participant> Participants {get; set;}
    }
}