using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Auxo.Application.Common.Interfaces;
using Auxo.Domain.Entities;

namespace Auxo.Persistence
{
    public class AuxoDbContext : DbContext, IAuxoDbContext {

        public AuxoDbContext(DbContextOptions<AuxoDbContext> opts) : base(opts)
        {

        }

        public DbSet<Team> Teams {get; set;}
        public DbSet<Profile> Profiles {get; set;}
        public DbSet<Participant> Participants {get; set;}
    }
}