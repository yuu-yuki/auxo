using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Auxo.Domain.Entities;

namespace Auxo.Persistence.Configurations
{
    public class ProfileConfiguration : IEntityTypeConfiguration<Profile> {
        public void Configure(EntityTypeBuilder<Profile> builder) {
            builder.HasKey(p => p.ProfileId);
            builder.Property(p => p.ProfileId)
                .IsRequired()
                .HasColumnName("ProfileId");
            
            builder.Property(p => p.Username)
                .IsRequired()
                .HasMaxLength(32)
                .HasColumnName("Username");
            
            builder.Property(p => p.Email)
                .IsRequired()
                .HasColumnName("Email");
            
            builder.Property(p => p.DisplayName)
                .IsRequired()
                .HasMaxLength(15)
                .HasColumnName("DisplayName");
            
            builder.Property(p => p.Bio).HasColumnType("ntext");

            builder.Property(p => p.Bio).HasColumnType("image");
        }
    }
}