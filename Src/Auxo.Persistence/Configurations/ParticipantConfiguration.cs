using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Auxo.Domain.Entities;

namespace Auxo.Persistence.Configurations
{
    public class ParticipantConfiguration : IEntityTypeConfiguration<Participant> {
        public void Configure(EntityTypeBuilder<Participant> builder) {
            builder.HasKey(p => p.ParticipantId);
            builder.Property(p => p.ParticipantId)
                .IsRequired();
            
            builder.Property(p => p.ProfileId).IsRequired();
            builder.Property(p => p.TeamId).IsRequired();

            builder.HasOne(p => p.Profile)
                .WithMany(p => p.Teams)
                .HasForeignKey(p => p.ProfileId)
                .HasConstraintName("FK_Profiles_Participants");
            
            builder.HasOne(p => p.Team)
                .WithMany(t => t.Participants)
                .HasForeignKey(p => p.ProfileId)
                .HasConstraintName("FK_Teams_Participants");
        }
    }
}