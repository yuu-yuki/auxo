using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Auxo.Domain.Entities;

namespace Auxo.Persistence.Configurations
{
    public class TeamConfiguration : IEntityTypeConfiguration<Team> 
    {
        public void Configure(EntityTypeBuilder<Team> builder) {
            builder.HasKey(p => p.TeamId);
            builder.Property(p => p.TeamId).HasColumnName("TeamId");
            builder.Property(p => p.TeamName)
                .IsRequired()
                .HasMaxLength(15)
                .HasColumnName("TeamName");
            
            builder.Property(p => p.Description).HasColumnType("ntext");
        }
    }
}