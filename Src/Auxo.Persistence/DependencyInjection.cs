using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Auxo.Application.Common.Interfaces;

namespace Auxo.Persistence
{
    public static class DependencyInjection {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration) {
            services.AddDbContext<AuxoDbContext>(opts => 
                opts.UseNpgsql(configuration.GetConnectionString("AuxoDB")));
            
            services.AddScoped<IAuxoDbContext>(provider => provider.GetService<AuxoDbContext>());
        
            return services;
        }
    }
}