using Microsoft.EntityFrameworkCore;
using Auxo.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Auxo.Application.Common.Interfaces
{
    public interface IAuxoDbContext
    {
        DbSet<Team> Teams {get; set;}
        DbSet<Profile> Profiles {get; set;}
        DbSet<Participant> Participants {get; set;}
    }
}