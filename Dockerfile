FROM mcr.microsoft.com/dotnet/core/sdk:3.1.201-alpine AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.sln .
COPY ./Src/Auxo.API/*.csproj              ./Src/Auxo.API/
COPY ./Src/Auxo.Application/*.csproj      ./Src/Auxo.Application/
COPY ./Src/Auxo.Domain/*.csproj           ./Src/Auxo.Domain/
COPY ./Src/Auxo.Infrastructure/*.csproj   ./Src/Auxo.Infrastructure/
COPY ./Src/Auxo.Persistence/*.csproj      ./Src/Auxo.Persistence/

COPY ./Tests/Auxo.API.IntegrationTests/*.csproj         ./Tests/Auxo.API.IntegrationTests/
COPY ./Tests/Auxo.Application.UnitTests/*.csproj        ./Tests/Auxo.Application.UnitTests/
COPY ./Tests/Auxo.Persistence.IntegrationTests/*.csproj ./Tests/Auxo.Persistence.IntegrationTests/
RUN dotnet restore

# Copy everything else and build
COPY ./Src/Auxo.API/            ./Src/Auxo.API/
COPY ./Src/Auxo.Application/    ./Src/Auxo.Application/
COPY ./Src/Auxo.Domain/         ./Src/Auxo.Domain/
COPY ./Src/Auxo.Infrastructure/ ./Src/Auxo.Infrastructure/
COPY ./Src/Auxo.Persistence/    ./Src/Auxo.Persistence/

COPY ./Tests/Auxo.API.IntegrationTests         ./Tests/Auxo.API.IntegrationTests/
COPY ./Tests/Auxo.Application.UnitTests        ./Tests/Auxo.Application.UnitTests/
COPY ./Tests/Auxo.Persistence.IntegrationTests ./Tests/Auxo.Persistence.IntegrationTests/

# publish env
FROM build-env AS publish-env
WORKDIR /app
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime-env
WORKDIR /app
COPY --from=publish-env /app/out .
ENTRYPOINT ["dotnet", "Auxo.API.dll"]